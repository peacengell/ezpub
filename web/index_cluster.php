<?php
/**
 * File containing the wrapper around the legacy index_cluster.php file
 *
 * @copyright Copyright (C) 1999-2014 eZ Systems AS. All rights reserved.
 * @license http://ez.no/licenses/gnu_gpl GNU General Public License v2.0
 */
$legacyRoot = '/mntraid/jenkins.std/jobs/ezpublish5-full-community/workspace/source/ezpublish5_community_project-v2014.03.2-gpl-full/ezpublish/../ezpublish_legacy';
chdir( $legacyRoot );
require $legacyRoot . '/index_cluster.php';

